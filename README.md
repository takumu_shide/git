# 研究成果

README
2021/02
実験ファイルを実行するための動作フローである．
【実験環境】
Ubuntu 16.04 、NS-3.29

【プログラムファイル説明】
◎シナリオファイル
シナリオファイルはソースコードが長く，ソースコードを変更するとコンパイルに時間がかかってしまうため，パラメータ毎にシナリオファイルを作成している．

――――――――――――――全ノード通信距離100m―――――――――――

normal-40km-rsu15.cc // rsu15台, AE区間

normal-40km-rsu15-x100-200.cc //rsu15台, AB区間

normal-40km-rsu15-x100-300.cc //rsu15台, AC区間

normal-40km-rsu15-x100-400.cc //rsu15台, AD区間

normal-40km-rsu15-x200.cc //rsu15台BE, 区間

normal-40km-rsu15-x300.cc //rsu15台CE, 区間

normal-40km-rsu15-x400.cc //rsu15台DE, 区間

normal-40km-rsu25.cc //rsu25台, AE区間

normal-40km-rsu25-x100-200.cc //rsu25台, AB区間

normal-40km-rsu25-x100-300.cc //rsu25台, AC区間

normal-40km-rsu25-x100-400.cc //rsu25台, AD区間

normal-40km-rsu25-x200.cc //rsu25台, BE区間

normal-40km-rsu25-x300.cc //rsu25台, CE区間

normal-40km-rsu25-x400.cc //rsu25台, DE区間

normal-40km-rsu35.cc //rsu35台, AE区間

normal-40km-rsu35-x100-200.cc //rsu35台, AB区間

normal-40km-rsu35-x100-300.cc //rsu35台, AC区間

normal-40km-rsu35-x100-400.cc //rsu35台, AD区間

normal-40km-rsu35-x200.cc //rsu35台, BE区間

normal-40km-rsu35-x300.cc //rsu35台, CE区間

normal-40km-rsu35-x400.cc //rsu35台, DE区間


――――――――――――――山頂RSU通信距離125m―――――――――――

decay-40km-125m-rsu15.cc // rsu15台, AE区間

decay-40km-125m-rsu15-x100-200.cc // rsu15台, AB区間

decay-40km-125m-rsu15-x100-300.cc // rsu15台, AC区間

decay-40km-125m-rsu15-x100-400.cc // rsu15台, AD区間

decay-40km-125m-rsu15-x200.cc // rsu15台, BE区間

decay-40km-125m-rsu15-x300.cc // rsu15台, CE区間

decay-40km-125m-rsu15-x400.cc // rsu15台, DE区間

decay-40km-125m-rsu25.cc // rsu25台, AE区間

decay-40km-125m-rsu25-x100-200.cc // rsu25台, AB区間

decay-40km-125m-rsu25-x100-300.cc // rsu25台, AC区間

decay-40km-125m-rsu25-x100-400.cc // rsu25台, AD区間

decay-40km-125m-rsu25-x200.cc // rsu25台, BE区間

decay-40km-125m-rsu25-x300.cc // rsu25台, CE区間

decay-40km-125m-rsu25-x400.cc // rsu25台, DE区間

decay-40km-125m-rsu35.cc // rsu35台, AE区間

decay-40km-125m-rsu35-x100-200.cc // rsu35台, AB区間

decay-40km-125m-rsu35-x100-300.cc // rsu35台, AC区間

decay-40km-125m-rsu35-x100-400.cc // rsu35台, AD区間

decay-40km-125m-rsu35-x200.cc // rsu35台, BE区間

decay-40km-125m-rsu35-x300.cc // rsu35台, CE区間

decay-40km-125m-rsu35-x400.cc // rsu35台, DE区間



――――――――――――――山頂RSU通信距離150m―――――――――――

decay-40km-150m-rsu15.cc // rsu15台, AE区間

decay-40km-150m-rsu15-x100-200.cc // rsu15台, AB区間

decay-40km-150m-rsu15-x100-300.cc // rsu15台, AC区間

decay-40km-150m-rsu15-x100-400.cc // rsu15台, AD区間

decay-40km-150m-rsu15-x200.cc // rsu15台, BE区間

decay-40km-150m-rsu15-x300.cc // rsu15台, CE区間

decay-40km-150m-rsu15-x400.cc // rsu15台, DE区間

decay-40km-150m-rsu25.cc // rsu25台, AE区間

decay-40km-150m-rsu25-x100-200.cc // rsu25台, AB区間

decay-40km-150m-rsu25-x100-300.cc // rsu25台, AC区間

decay-40km-150m-rsu25-x100-400.cc // rsu25台, AD区間

decay-40km-150m-rsu25-x200.cc // rsu25台, BE区間

decay-40km-150m-rsu25-x300.cc // rsu25台, CE区間

decay-40km-150m-rsu25-x400.cc // rsu25台, DE区間

decay-40km-150m-rsu35.cc // rsu35台, AE区間

decay-40km-150m-rsu35-x100-200.cc // rsu35台, AB区間

decay-40km-150m-rsu35-x100-300.cc // rsu35台, AC区間

decay-40km-150m-rsu35-x100-400.cc // rsu35台, AD区間

decay-40km-150m-rsu35-x200.cc // rsu35台, BE区間

decay-40km-150m-rsu35-x300.cc // rsu35台, CE区間

decay-40km-150m-rsu35-x400.cc // rsu35台, DE区間



◎プロトコルファイル

shide-normal-routing-protocol.cc

shide-normal-routing-protocol.h

shide-normal-packet.cc

shide-normal-packet.h



【ファイル構成】

ns-3.29
  
  　scratch

   　　 シナリオファイル一式

src

　　shide-normal

　　　　model

　　　　　　shide-normal-routing-protocol.cc

　　　　　　shide-normal-routing-protocol.h

　　　　　　shide-normal-packet.cc

　　　　　　shide-normal-packet.h



【実行手順】

①	shide-normalファイルを”/ns-3.29/bake/source/ns-3.29/src”ディレクトリ下へ置く．

また，scratchファイルを”ns-3.29/bake/source/ns-3.29”ディレクトリ下へ置く．



② Ubuntu端末で以下を実行

$ cd

$ cd ns-3.29/bake/source/ns-3.29

$ ./waf --configure

$ ./waf build 



3. プログラムの実行(デフォルトではMFMR)

ここではdecay-40km-150m-rsu35-x400.ccのシナリオファイルを実行．

$ ./waf --run decay-40km-150m-rsu35-x400



4. 動作確認

$ cd

$ cd ns-3.29/bake/source/netanim-3.109

$ ./NetAnim


左上のファイルのボタンを左クリック

ファイル選択の画面が出力されるのでFale_nameに”deccy-40km.xml”を選択肢し右下のOpenを左クリック

シナリオアニメーションの画面が出力されれば動作確認成功





5．フラッディング方式の変更(pure, probability, FMR, MFMRのどの方式でプログラムを実行するか)

shide-normal/modelディレクトリ下にあるshide-normal-routing-protocol.ccのソースコード 437行目にあるRecvShideクラスの中身を下記の通り
に変更しプログラムの実行を行う．また，pure, probabilityはマクロ変数（ソースコード25行目）についても変更を行う．

	pureの場合

RecvShideクラス ソースコード437行目

RecvPureBroadcast();

//RecvWindingBroadcast();

//RecvWinding2Broadcast();

//RecvWinding3Broadcast();



マクロ変数 ソースコード25行目　

#define PROBABILITY 100 //パケット転送確率



	probabilityの場合

RecvShideクラス ソースコード437行目

RecvPureBroadcast();

//RecvWindingBroadcast();

//RecvWinding2Broadcast();

//RecvWinding3Broadcast();



マクロ変数 ソースコード25行目　

#define PROBABILITY 75 //パケット転送確率

or

#definr PROBABILITY 50 //パケット転送確率





	FMRの場合

RecvShideクラス ソースコード437行目

RecvPureBroadcast();

//RecvWindingBroadcast();

//RecvWinding2Broadcast();

//RecvWinding3Broadcast();



	FMRの場合

RecvShideクラス ソースコード437行目

RecvPureBroadcast();

//RecvWindingBroadcast();

//RecvWinding2Broadcast();

//RecvWinding3Broadcast();



任意のフラッディング方式に変更後，シナリオファイルを実行する．

例）$ ./waf --run decay-40km-150m-rsu35-x400





6. デフォルト（decay-40km-150m-rsu35-x400）以外のシナリオファイルを実行する場合


shide-normal-routing-protocol.ccファイルの24〜37行目を各行のコメントに従い，実行したいシナリオファイルに沿ってパラメータを変更する．

―――――――――――――下記ソースコード24〜37行目――――――――――――

#define Nnumber 44 //全ノード数(RSU+車両) 44 or 34 or 24

#define PROBABILITY 100 //rebraodcast %

#define MOUNTAIN_RSU_RANGE 22500  //山頂RSUの通信範囲 10000 = 100m, 15625 = 125m, 122500 = 150m 

#define NEGATIVE_MOUNTAIN_RSU_RANGE -22500

#define HYOUKA_MOUNTAIN_RSU_RANGE 22500 //山頂RSUの通信範囲 10000 = 100m, 15625 = 125m, 22500 = 150m 

#define HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE -22500

#define HYOUKA 1 //1 = 対向車両と総パケット数, 2 = 後方車両

#define HYOKA_KUKAN 400 //左発の車両がどのx座標を通過したら評価を開始するか 100 = 左端, 200 = 左端と山頂の間, 300 = 山頂, 400 = 右端
と山頂の間

#define HYOUKA_END_DISTANCE 0 // 評価をどこまで取るか（左発と右発の先頭車両の距離で判別）0 = 0m,10000 =100m

#define NOMAL_NODE_RANGE 10000 //通常RSUの通信範囲

#define NEGATIVE_NOMAL_NODE_RANGE -10000 //通常RSUの通信範囲

#define UPHILL_RANGE 4444 //通常RSUの上り通信範囲

#define NEGATIVE_UPHILL_RANGE -4444 //通常RSUの上り通信範囲

――――――――――――――――――――――――――――――――――――――

パラメータ変更後 （$ ./waf --run “実行シナリオファイル名”） で実行する．





例１）	normal-40km-rsu15.cc // rsu15台, AE区間を実行する

#define Nnumber 24 //全ノード数(RSU+車両) 44 or 34 or 24

#define PROBABILITY 100 //rebraodcast %

#define MOUNTAIN_RSU_RANGE 10000  //山頂RSUの通信範囲 10000 = 100m, 15625 = 125m, 22500 = 150m 

#define NEGATIVE_MOUNTAIN_RSU_RANGE -10000

#define HYOUKA_MOUNTAIN_RSU_RANGE 10000 //山頂RSUの通信範囲 10000 = 100m, 15625 = 125m, 22500 = 150m 

#define HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE -10000

#define HYOUKA 1 //1 = 対向車両と総パケット数, 2 = 後方車両

#define HYOKA_KUKAN 100 //左発の車両がどのx座標を通過したら評価を開始するか 100 = 左端, 200 = 左端と山頂の間, 300 = 山頂, 400 = 右端
と山頂の間

#define HYOUKA_END_DISTANCE 0 // 評価をどこまで取るか（左発と右発の先頭車両の距離で判別）0 = 0m,10000 =100m

#define NOMAL_NODE_RANGE 10000 //通常RSUの通信範囲

#define NEGATIVE_NOMAL_NODE_RANGE -10000 //通常RSUの通信範囲

#define UPHILL_RANGE 4444 //通常RSUの上り通信範囲

#define NEGATIVE_UPHILL_RANGE -4444 //通常RSUの上り通信範囲

上記にパラメータを変更し，

$ ./waf –run normal-40km-rsu15

で実行する．






例２）	decay-40km-125m-rsu35-x400.cc // rsu35台, DE区間を実行する

#define Nnumber 44 //全ノード数(RSU+車両) 44 or 34 or 24

#define PROBABILITY 100 //rebraodcast %

#define MOUNTAIN_RSU_RANGE 15625 //山頂RSUの通信範囲 10000 = 100m, 15625 = 125m, 22500 = 150m 

#define NEGATIVE_MOUNTAIN_RSU_RANGE -15625

#define HYOUKA_MOUNTAIN_RSU_RANGE 15625 //山頂RSUの通信範囲 10000 = 100m, 15625 = 125m, 22500 = 150m 

#define HYOUKA_NEGATIVE_MOUNTAIN_RSU_RANGE -15625

#define HYOUKA 1 //1 = 対向車両と総パケット数, 2 = 後方車両

#define HYOKA_KUKAN 400 //左発の車両がどのx座標を通過したら評価を開始するか 100 = 左端, 200 = 左端と山頂の間, 300 = 山頂, 400 = 右端
と山頂の間

#define HYOUKA_END_DISTANCE 0 // 評価をどこまで取るか（左発と右発の先頭車両の距離で判別）0 = 0m,10000 =100m

#define NOMAL_NODE_RANGE 10000 //通常RSUの通信範囲

#define NEGATIVE_NOMAL_NODE_RANGE -10000 //通常RSUの通信範囲

#define UPHILL_RANGE 4444 //通常RSUの上り通信範囲

#define NEGATIVE_UPHILL_RANGE -4444 //通常RSUの上り通信範囲

上記にパラメータを変更し，

$ ./waf –run decay-40km-125m-rsu35-x400

で実行する．

